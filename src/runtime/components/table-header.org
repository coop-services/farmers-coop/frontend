#+TITLE: Table Header
#+AUTHOR: Thirumal Ravula
#+DATE: [2019-05-17 Fri]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Table Header
This component provides the the header row for displaying
the rows of entity.  The columns that are displayed by this
component are sent as props by the calling component.

#+BEGIN_SRC vue :tangle TableHeader.vue
<template>
  <v-card flat>
    <v-layout align-start justify-space-between row wrap :class="`pa-3 product`">
      <v-flex xs4 md2 lg2>
        <div class="caption grey--text"> {{ headrow.first }} </div>
      </v-flex>
      <v-flex xs8 md5 lg5>
        <div class="caption grey--text"> {{ headrow.second }} </div>
      </v-flex>
      <v-flex xs12 md3 lg3>
        <div class="caption grey--text"> {{ headrow.third }}</div>
      </v-flex>
    </v-layout>
    <v-divider></v-divider>
  </v-card>
</template>

<script>
export default {
  props: {
    headrow: {
      type: Object,
      required: true
    }
  },
  
  data() {
    return {

    }
  }
}
</script>


#+END_SRC

