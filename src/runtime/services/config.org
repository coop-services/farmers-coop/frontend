#+TITLE: Configurations for Services
#+AUTHOR: Thirumal Ravula
#+DATE: [2019-05-27 Mon]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
Configuration parameters are defined here for the services.


* Application Url

This is the url of the back end application that exposes the
REST API.

#+NAME: app-url
#+BEGIN_SRC js
var appUrl = 'http://10.0.8.165:3000';

#+END_SRC
* API Version
This is the version of the REST API.

#+NAME: api-version
#+BEGIN_SRC js
var apiVersion = "api";

#+END_SRC


* Exports
#+NAME: exports
#+BEGIN_SRC js
export { appUrl, apiVersion };

#+END_SRC
* Tangle
#+BEGIN_SRC js :tangle config.js :eval no :noweb yes
<<app-url>>
<<api-version>>
<<exports>>
#+END_SRC
